//
//  Endpoint.swift
//  App
//
//  Created by Micah A. Walles on 7/24/18.
//

import Vapor

protocol Endpoint {

	associatedtype ReturnType: ResponseEncodable

	/// HTTP request method
	var method: HTTPMethod { get }

	/// The verison of this endpoint
	var version: Int { get }

	/// URI path
	var path: [PathComponentsRepresentable] { get }

	/// Any middleware used on this individual route rather than across the whole Service.
	var middleware: [Middleware] { get set }

	/// The closure that is executed when the route is hit
	var handler: (Request) throws -> Future<ReturnType> { get }
}

extension Endpoint {

	/// Method to add this `Endpoint` to the server
	///
	/// - Parameters:
	///     - router: The `Router` that this endpoint needs to ba added to
	public func add(to router: Router) {
		router.grouped("\(version)")				// Adding the version number to the URI
			.grouped(middleware)					// Adding any middleware that are needed for this endpoint
			.on(method, at: path, use: handler)		// Adding the rest of the path and setting what method will
													// be use as well as setting the code that will be used when the
													// route is hit
	}
}
