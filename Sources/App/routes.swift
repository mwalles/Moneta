import Vapor
import Leaf

/// Register your application's routes here.
public func routes(_ router: Router) throws {

	// MARK: - View Endpoints
	router.get("/") { request -> Future<View> in

		let dateFilter = try request.query.decode(DateFilter.self)
		let transactions = try Transaction.getTransactions(in: dateFilter, on: request)
		let categories = Category.query(on: request).all()
		let (start, _) = try dateFilter.startAndEnd()
		let components = Calendar.current.dateComponents(Set([.year, .month, .day]), from: start)

		guard let year = components.year,
			let month = components.month,
			let day = components.day else {
				throw Abort(.internalServerError)
		}

		let pizzaMoney = try PizzaMoney.getPizzaMoney(for: "\(year)-\(month)-\(day)", on: request)

		return flatMap(to: View.self, transactions, categories, pizzaMoney) { (transactions, categories, pizzaMoney) -> Future<View> in
			return try categories.compactMap { (category) -> Future<CategoryContent>? in
				guard let categoryId = category.id else {
					return nil
				}
				return try Transaction.total(in: dateFilter, for: categoryId, on: request)
					.map(to: CategoryContent.self) { total -> CategoryContent in
						return CategoryContent(id: categoryId,
											   total: total,
											   amount: category.amount,
											   name: category.name,
											   color: category.color)
				}
			}.flatten(on: request)
				.flatMap({ (categoryContents) -> Future<View> in
					let content = UIContent(transactions: transactions,
											categories: categoryContents,
											pizzaMoney:pizzaMoney.amount)
					return try request.view().render("main", content)
				})
		}
	}

	// MARK: - API Endpoints
	// TODO: should add a middleware to handle authentication.
	// This really depends on the type of authentication that will be used.
	let api = router.grouped("api")

	// MARK: Category
	let categoryGroup = api.grouped("category")
	PutCategoryV1().add(to: categoryGroup)
	PatchCategoryV1().add(to: categoryGroup)
	DeleteCategoryV1().add(to: categoryGroup)
	GetCategoriesV1().add(to: categoryGroup)
	GetCategoryV1().add(to: categoryGroup)
	GetCategoryTransactionsV1().add(to: categoryGroup)
	GetCategoryTransactionsTotalV1().add(to: categoryGroup)

	// MARK: Transaction
	let transactionGroup = api.grouped("transaction")
	PutTransactionV1().add(to: transactionGroup)
	PatchTransactionV1().add(to: transactionGroup)
	DeleteTransactionV1().add(to: transactionGroup)
	GetTransactionsV1().add(to: transactionGroup)
	GetTransactionV1().add(to: transactionGroup)
	GetTransactionsForCategoryV1().add(to: transactionGroup)

	// MARK: Pizza Money
	let pizzaMoneyGroup = api.grouped("pizzaMoney")
	GetPizzaMoneyV1().add(to: pizzaMoneyGroup)
	// get ammount for month
}
