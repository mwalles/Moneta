//
//  Transaction.swift
//  App
//
//  Created by Micah A. Walles on 7/23/18.
//

import FluentSQLite
import Vapor

enum TransactionError: Error {
	case idNotFound
}

final class Transaction: SQLiteUUIDModel {
	var id: Transaction.ID?

	/// The amount that this `Transaction` was for
	var amount: Double
	/// Who recived the money from this `Transaction`
	var paidTo: String
	/// What `Category` this `Transaction` is a part of
	var categoryId: Category.ID

	/// The date that this `Transaction` occurred on
	var transactionDate: Date

	/// Date the record in the database was created
	var created: Date?
	/// Date the record in the datebase was update
	var updated: Date?
	/// Date the record in the datebase was deleted
	/// Note: No record will be fully deleted, only "soft" deleted
	var deleted: Date?

	init(amount: Double, paidTo: String, categoryId: Category.ID, transactionDate: Date) {
		self.paidTo = paidTo
		self.amount = amount
		self.categoryId = categoryId
		self.transactionDate = transactionDate
	}

	// MARK: - Vapor overrides for timestamps

	static var createdAtKey: TimestampKey? { return \.created }
	static var updatedAtKey: TimestampKey? { return \.updated }
	static var deletedAtKey: TimestampKey? { return \.deleted }

	static func getTransactions(in dateFilter: DateFilter,
								for categoryId: Category.ID? = nil,
								on database: DatabaseConnectable) throws -> Future<[Transaction]> {
		let (start, end) = try dateFilter.startAndEnd()
		var transactionQuery = Transaction.query(on: database)
			.filter(\.transactionDate > start)
			.filter(\.transactionDate < end)

		if let categoryId = categoryId {
			transactionQuery =  transactionQuery.filter(\.categoryId == categoryId)
		}

		return transactionQuery.all()
	}

	static func total(in dateFilter: DateFilter,
					  for categoryId: Category.ID,
					  on database: DatabaseConnectable) throws -> Future<Double> {
		let (start, end) = try dateFilter.startAndEnd()

		let baseQuery = Transaction.query(on: database)
			.filter(\.transactionDate > start)
			.filter(\.transactionDate < end)
			.filter(\.categoryId == categoryId)

		return baseQuery.count()
			.flatMap(to: Double.self) { count -> Future<Double> in
				if count == 0 {
					return database.future(0)
				}
				return baseQuery.sum(\.amount)
			}
	}
}

/// Allows `Transaction` to be used as a dynamic migration.
extension Transaction: Migration { }

/// Allows `Transaction` to be encoded to and decoded from HTTP messages.
extension Transaction: Content { }

/// Allows `Transaction` to be used as a dynamic parameter in route definitions.
extension Transaction: Parameter { }
