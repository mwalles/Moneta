//
//  UIModels.swift
//  App
//
//  Created by Micah A. Walles on 7/31/18.
//

import Vapor

struct UIContent: Content {
	let transactions: [Transaction]
	let categories: [CategoryContent]
	let pizzaMoney: Double
}

struct CategoryContent: Content {
	let id: Category.ID
	let total: Double
	let amount: Double
	let name: String
	let color: String
}
