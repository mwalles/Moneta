//
//  Category.swift
//  App
//
//  Created by Micah A. Walles on 7/23/18.
//

import FluentSQLite
import Vapor

enum CategoryError: Error {
	case idNotFound
}

final class Category: SQLiteUUIDModel {

	/// The unique identifier for a `Category`
	var id: Category.ID?

	/// The name that the user has given this `Category`
	var name: String
	/// The total amount of money that has been budgeted to this `Category` for a time frame
	var amount: Double
	/// A color to help differentiate Categories when they are displayed
	var color: String

	// A future enhance that would allow a user to determine how far under budget they need to
	// be before the extra money can count to the Pizza Money feature
	//var pizzaMoneyThreshold: Double

	/// Date the record in the database was created
	var created: Date?
	/// Date the record in the datebase was update
	var updated: Date?
	/// Date the record in the datebase was deleted
	/// Note: No record will be fully deleted, only "soft" deleted
	var deleted: Date?

	init(name: String, amount: Double, color: String) {
		self.name = name
		self.amount = amount
		self.color = color
	}

	// MARK: - Vapor overrides for timestamps
	static var createdAtKey: TimestampKey? { return \.created }
	static var updatedAtKey: TimestampKey? { return \.updated }
	static var deletedAtKey: TimestampKey? { return \.deleted }

}

/// Allows `Category` to be used as a dynamic migration.
extension Category: Migration { }

/// Allows `Category` to be encoded to and decoded from HTTP messages.
extension Category: Content { }

/// Allows `Category` to be used as a dynamic parameter in route definitions.
extension Category: Parameter { }
