//
//  PizzaMoney.swift
//  App
//
//  Created by Micah A. Walles on 7/27/18.
//

import FluentSQLite
import Vapor

enum PizzaMoneyError: Error {
	case idNotFound
}

final class PizzaMoney: SQLiteUUIDModel {
	var id: PizzaMoney.ID?

	/// The time period that this entry is for. This is stored as the Date for the first day of the time period
	var	timePeriod: Date
	/// The amount that was saved
	var amount: Double

	/// Date the record in the database was created
	var created: Date?

	init(timePeriod: Date, amount: Double) {
		self.timePeriod = timePeriod
		self.amount = amount
	}

	static var createdAtKey: TimestampKey? { return \.created }

	static func getPizzaMoney(for dateString: String, on database: DatabaseConnectable) throws -> Future<PizzaMoney> {
		let (start, end) = try DateFilter.startAndEndFor(timePeriod: dateString)

		let compare = Calendar.current.dateComponents(Set(arrayLiteral: .year, .month), from: start, to: Date())

		guard let monthDiff = compare.month,
			let yearDiff = compare.year,
			(yearDiff > 0 || (yearDiff == 0 && monthDiff > 0)) else {
				// Dif the date passed in is for the current or future time period the amount returned is 0
				return database.future(PizzaMoney(timePeriod: start, amount: 0))
		}

		let pizza = PizzaMoney.query(on: database)
			.filter(\.timePeriod == start)
			.first()
		return pizza.flatMap(to: PizzaMoney.self) { pizzaMoney -> Future<PizzaMoney> in
			if let pizzaMoney = pizzaMoney {
				return database.future(pizzaMoney)
			}
			return Category.query(on: database)
				.all()
				.flatMap(to: PizzaMoney.self) { categories -> Future<PizzaMoney> in
					var sums = [Future<Double>]()
					for category in categories {
						guard let categoryId = category.id else {
							continue
						}
						let transactionBase = Transaction.query(on: database)
							.filter(\.transactionDate > start)
							.filter(\.transactionDate < end)
							.filter(\.categoryId == categoryId)

						let sum = transactionBase.count()
							.flatMap(to: Double.self) { count -> Future<Double> in
								if count > 0 {
									return transactionBase
										.sum(\.amount)
										.map(to: Double.self) { sum -> Double in
											// Check that the total form the transactions is less then the amount assigned to that category
											let total = category.amount - sum
											if total <= 0 {
												return 0
											}
											return total
									}
								}
								return database.future(0)
						}
						sums.append(sum)
					}
					let flatSums = sums.flatten(on: database)
					return flatSums.flatMap(to: PizzaMoney.self) { sums -> Future<PizzaMoney> in
						// Add together the sums for each of the different categories
						let sum = sums.reduce(0, { x, y in
							x + y
						})
						return PizzaMoney(timePeriod: start, amount: sum).save(on: database)
					}
			}
		}
	}
}

/// Allows `PizzaMoney` to be used as a dynamic migration.
extension PizzaMoney: Migration { }

/// Allows `PizzaMoney` to be encoded to and decoded from HTTP messages.
extension PizzaMoney: Content { }
