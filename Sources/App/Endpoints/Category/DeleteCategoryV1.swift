//
//  DeleteCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// Soft deletes a `Category`
struct DeleteCategoryV1: Endpoint {

	var method: HTTPMethod = .DELETE

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["category", Category.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<HTTPStatus> = { (request) throws -> Future<HTTPStatus> in
		// This will take the data in from the request and convert it to a new Category model.
		return try request.parameters.next(Category.self)
			.map(to: Bool.self) { category -> Bool in
				_ = category.delete(on: request)
				return true
			}
			.transform(to: HTTPStatus.accepted)
	}
}
