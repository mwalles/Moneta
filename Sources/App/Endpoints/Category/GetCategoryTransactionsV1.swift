//
//  GetCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Get a list of `Transaction`s for the `Category` for the time frame that was passed in
struct GetCategoryTransactionsV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

//	var path: [PathComponentsRepresentable] = ["category", UUID.parameter, "transaction"]
	// FIXME: Need to put a ticket off to Vapor to see why having the parameter in the middle isn't working
	var path: [PathComponentsRepresentable] = ["category", "transaction", UUID.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<[Transaction]> = { (request) throws -> Future<[Transaction]> in
		let categoryId = try request.parameters.next(UUID.self)
		let dateFilter = try request.query.decode(DateFilter.self)
		return try Transaction.getTransactions(in: dateFilter, for: categoryId, on: request)

	}
}
