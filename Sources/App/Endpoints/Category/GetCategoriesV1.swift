//
//  GetCategoriesV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Gets a list of `Category`s
struct GetCategoriesV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["category"]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<[Category]> = { (request) throws -> Future<[Category]> in
		return Category
			.query(on: request)
			.all()
	}
}
