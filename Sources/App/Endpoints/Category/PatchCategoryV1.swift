//
//  PatchCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// An endpoint to update a `Category`
/// - Attention: At some point, this might need to be changed so that when a `Category`
/// is updated, a new Category is created and the current one is soft deleted if the
/// amount is changed. This way we can keep better historical records as to if a `Category`
/// was over or under budget
struct PatchCategoryV1: Endpoint {

	var method: HTTPMethod = .PATCH

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["category", Category.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<HTTPStatus> = { (request) throws -> Future<HTTPStatus> in
		// This will take the data in from the request and convert it to a Category model so that it is easier to pull the data out.
		return try request.content.decode(Category.self)
			.flatMap(to: Category.self) { update -> Future<Category> in
				// This will grab the Category identified by ID passed into the endpoint
				return try request.parameters.next(Category.self)
					.flatMap(to: Category.self) { category -> Future<Category> in

						category.name = update.name
						category.amount = update.amount
						category.color = update.color

						return category.save(on: request)
				}
			}
			.transform(to: HTTPStatus.accepted)
	}
}
