//
//  GetCategoryTransactionsTotal.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Returns the total abount spent on `Transaction`s in this `Category` for the time frame passed in the query string
struct GetCategoryTransactionsTotalV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

//	var path: [PathComponentsRepresentable] = ["category", UUID.parameter, "transaction", "total"]
	// FIXME: Need to put a ticket off to Vapor to see why having the parameter in the middle isn't working
	var path: [PathComponentsRepresentable] = ["category", "transaction", "total", UUID.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<Double> = { (request) throws -> Future<Double> in
		let categoryId = try request.parameters.next(UUID.self)
		let dateFilter = try request.query.decode(DateFilter.self)
		return try Transaction.total(in: dateFilter, for: categoryId, on: request)
	}
}
