//
//  GetCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Get a signle `Category` based on the UUID passed in
struct GetCategoryV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["category", Category.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<Category> = { (request) throws -> Future<Category> in
		return try request.parameters.next(Category.self)
	}
}
