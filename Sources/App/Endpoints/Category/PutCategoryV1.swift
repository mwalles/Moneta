//
//  PutCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// Creates a new `Category`. Returns a JSON object with the UUID for the new `Category`
struct PutCategoryV1: Endpoint {

	struct CategorySave: Content {
		let categoryId: Category.ID
	}

	var method: HTTPMethod = .POST

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["category"]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<CategorySave> = { (request) throws -> Future<CategorySave> in
		// This will take the data in from the request and convert it to a new Category model.
		return try request.content.decode(Category.self)
			.flatMap(to: Category.self) { category -> Future<Category> in
				// If there is an error saving to the DB, there will be an error that is throw. That
				// error will be captured by the future. Vapor will convert the error into JSON to
				// send to the client. Other wise it will return our model as JSON
				return category.save(on: request)
			}
			.map(to: CategorySave.self) { category in
				guard let categoryId = category.id else {
					// This should never happen, but just incase there is no unique id for thd
					// model we just saved we will throw an error
					throw CategoryError.idNotFound
				}
				return CategorySave(categoryId: categoryId)
//				return CategorySave(categoryId: categoryId.uuidString)
		}
	}
}
