//
//  GetPizzaMoneyV1.swift
//  App
//
//  Created by Micah A. Walles on 7/27/18.
//

import Vapor
import Fluent

/// Gets the `PizzaMoney` object for the time period passed in as a query string.
/// - Note: If there is entry in the PizzaMoney table for this timeperiod it will return that.
/// Otherwise it will create a new entry to save into the database and return that value.
struct GetPizzaMoneyV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	// The string parameter should be in the format of YYYY-MM-DD
	var path: [PathComponentsRepresentable] = ["pizzaMoney", String.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<PizzaMoney> = { (request) throws -> Future<PizzaMoney> in
		let dateString = try request.parameters.next(String.self)
		return try PizzaMoney.getPizzaMoney(for: dateString, on: request)
	}
}
