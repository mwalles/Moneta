//
//  DateFilter.swift
//  App
//
//  Created by Micah A. Walles on 7/28/18.
//

import Vapor

/// An object to decode a set of values from a query string
/// - Note: If timePeriod is included the other values will be ignored.
/// If only end is included it will be ignored
struct DateFilter: Content {
	/// The date of the first day in the time period in the format: YYYY-MM-DD
	let timePeriod: String?
	/// The start date of the user defined date range in the format: YYYY-MM-DD
	let start: String?
	/// The end date of the user defined date range in the format: YYYY-MM-DD
	let end: String?

	/// Returns the start and end dates for the range of intrests.
	/// - Returns: a tuple of dates for the start and end of the range
	func startAndEnd() throws -> (start: Date, end: Date) {
		if let timePeriod = self.timePeriod {
			return try DateFilter.startAndEndFor(timePeriod: timePeriod)
		} else if let start = self.start {
			let startDate = try DateFilter.date(for: start)
			let endDate: Date
			if let end = self.end {
				endDate = try DateFilter.date(for: end)
			} else {
				endDate = Date()
			}
			return (startDate, endDate)
		}
		let components = Calendar.current.dateComponents(Set([.year, .month, .day]), from: Date())

		guard let year = components.year,
			let month = components.month,
			let day = components.day else {
				throw Abort(.internalServerError)
		}
		return try DateFilter.startAndEndFor(timePeriod: "\(year)-\(month)-\(day)")
	}

	/// Converts a string in the format of `YYYY-MM-DD` into a date
	/// - Parameters:
	///		- dateString: a string representation of a date in the format of `YYYY-MM-DD`
	/// - Returns: A date
	/// - Throws: A bad request if it can not get the components of the date or it fails to make a date
	static func date(for dateString: String) throws -> Date {
		let dateArray = dateString.split(separator: "-")

		guard dateArray.count == 3,
			let year = Int(dateArray[0]),
			let month = Int(dateArray[1]),
			let day = Int(dateArray[2]) else {
				throw Abort(.badRequest)
		}

		let dateComponents = DateComponents(calendar: Calendar.current, year: year, month: month, day: day)

		guard let date = dateComponents.date else {
			throw Abort(.badRequest)

		}

		return date
	}

	/// Returns the start and end dates for a time period.
	/// - Parameters:
	///		- timePeriod: a string representation of a date in the format of `YYYY-MM-DD`
	/// - Note: At some point in the future it might be a good feature to allow the time period to be user defined.
	/// At that time this method will need to be update so that it will look up what the correct time frame is.
	/// - Returns: a tuple of dates for the start and end of the range
	/// - Throws: A bad request if it can not get the date or create an end date
	static func startAndEndFor(timePeriod: String) throws -> (start: Date, end: Date) {
		let start = try date(for: timePeriod)

		// If the time period ever changes from month this will need to be updated to add to the appropriate component
		guard let end = Calendar.current.date(byAdding: .month, value: 1, to: start) else {
				throw Abort(.badRequest)
		}

		return (start, end)
	}

}
