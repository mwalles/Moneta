//
//  DeleteTransactionV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// Soft deletes a `Transaction`
struct DeleteTransactionV1: Endpoint {

	var method: HTTPMethod = .DELETE

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction", Transaction.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<HTTPStatus> = { (request) throws -> Future<HTTPStatus> in
		// This will take the data in from the request and convert it to a new Transaction model.
		return try request.parameters.next(Transaction.self)
			.map(to: Bool.self) { transaction -> Bool in
				_ = transaction.delete(on: request)
				return true
			}
			.transform(to: HTTPStatus.accepted)
	}
}
