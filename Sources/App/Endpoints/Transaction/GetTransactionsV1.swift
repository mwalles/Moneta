//
//  GetTransactionsV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Gets a list of `Transaction`s based on the time frame that was passed in
struct GetTransactionsV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction"]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<[Transaction]> = { (request) throws -> Future<[Transaction]> in
		let dateFilter = try request.query.decode(DateFilter.self)
		return try Transaction.getTransactions(in: dateFilter, on: request)
	}
}
