//
//  GetTransactionsForCategoryV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Gets a list of `Transaction`s for a Category based on the time frame that was passed in
struct GetTransactionsForCategoryV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction", "category", UUID.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<[Transaction]> = { (request) throws -> Future<[Transaction]> in
		let categoryId = try request.parameters.next(UUID.self)
		let dateFilter = try request.query.decode(DateFilter.self)
		return try Transaction.getTransactions(in: dateFilter, for: categoryId, on: request)
	}
}
