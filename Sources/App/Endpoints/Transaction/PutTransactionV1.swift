//
//  PutTransactionV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// An endpoint to create a `Transaction`. Returns a JSON object with the UUID for the new category
struct PutTransactionV1: Endpoint {

	struct TransactionSave: Content {
		let transactionId: String
	}

	var method: HTTPMethod = .POST

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction"]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<TransactionSave> = { (request) throws -> Future<TransactionSave> in
		// This will take the data in from the request and convert it to a new Transaction model.
		return try request.content.decode(Transaction.self)
			.flatMap(to: Transaction.self) { transaction -> Future<Transaction> in
				// If there is an error saving to the DB, there will be an error that is throw. That
				// error will be captured by the future. Vapor will convert the error into JSON to
				// send to the client. Other wise it will return our model as JSON
				return transaction.save(on: request)
			}
			.map(to: TransactionSave.self) { transaction in
				guard let transactionId = transaction.id else {
					// This should never happen, but just incase there is no unique id for thd
					// model we just saved we will throw an error
					throw TransactionError.idNotFound
				}
				return TransactionSave(transactionId: transactionId.uuidString)

		}
	}
}
