//
//  GetTransactionV1.swift
//  App
//
//  Created by Micah A. Walles on 7/26/18.
//

import Vapor
import Fluent

/// Gets a `Transaction` based on the UUID passed in
struct GetTransactionV1: Endpoint {

	var method: HTTPMethod = .GET

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction", Transaction.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<Transaction> = { (request) throws -> Future<Transaction> in
		return try request.parameters.next(Transaction.self)
	}
}
