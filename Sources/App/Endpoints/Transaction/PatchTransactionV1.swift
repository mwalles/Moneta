//
//  PatchTransactionV1.swift
//  App
//
//  Created by Micah A. Walles on 7/25/18.
//

import Vapor
import Fluent

/// An endpoint to update a `Transaction`
struct PatchTransactionV1: Endpoint {

	var method: HTTPMethod = .PATCH

	var version: Int = 1

	var path: [PathComponentsRepresentable] = ["transaction", Transaction.parameter]

	var middleware: [Middleware] = []

	var handler: (Request) throws -> Future<HTTPStatus> = { (request) throws -> Future<HTTPStatus> in
		// This will take the data in from the request and convert it to a Transaction model so that it is easier to pull the data out.
		return try request.content.decode(Transaction.self)
			.flatMap(to: Transaction.self) { update -> Future<Transaction> in
				// This will grab the Transaction identified by ID passed into the endpoint
				return try request.parameters.next(Transaction.self)
					.flatMap(to: Transaction.self) { transaction -> Future<Transaction> in

						transaction.amount = update.amount
						transaction.paidTo = update.paidTo
						transaction.categoryId = update.categoryId
						transaction.transactionDate = update.transactionDate

						return transaction.save(on: request)
				}
			}
			.transform(to: HTTPStatus.accepted)
	}
}
