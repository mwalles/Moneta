import FluentSQLite
import Leaf
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
	/// Register providers first
	try services.register(FluentSQLiteProvider())

	/// Register routes to the router
	let router = EngineRouter.default()
	try routes(router)
	services.register(router, as: Router.self)

	/// Register middleware
	var middlewares = MiddlewareConfig() // Create _empty_ middleware config
	middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
	middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
	services.register(middlewares)

	/// Leaf Confiuration
	try services.register(LeafProvider())
	config.prefer(LeafRenderer.self, for: ViewRenderer.self)
	var tags = LeafTagConfig.default()
	tags.use(Raw(), as: "raw")
	services.register(tags)


	let database = "/Users/micah/Projects/Spreetail/Moneta/Database/Moneta.db"

	// Configure a SQLite database
	var databases = DatabasesConfig()
	try databases.add(database: SQLiteDatabase(storage: .file(path: database)), as: .sqlite)
//	databases.enableLogging(on: .sqlite) // Uncomment this line to debug SQL issues
	services.register(databases)

	/// Configure migrations
	var migrations = MigrationConfig()
	migrations.add(model: Category.self, database: .sqlite)
	migrations.add(model: Transaction.self, database: .sqlite)
	migrations.add(model: PizzaMoney.self, database: .sqlite)
	services.register(migrations)

}
