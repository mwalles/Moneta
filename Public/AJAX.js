function loadJSON(url, method, body, handler){
	 var httpRequest = new XMLHttpRequest();
	 try{
			// Opera 8.0+, Firefox, Chrome, Safari
			httpRequest = new XMLHttpRequest();
	 }catch (e){
			// Internet Explorer Browsers
			try{
				 httpRequest = new ActiveXObject("Msxml2.XMLHTTP");

			}catch (e) {

				 try{
						httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
				 }catch (e){
						// Something went wrong
						alert("Your browser broke!");
						return false;
				 }

			}
	 }

	httpRequest.onreadystatechange = function(){

			if (httpRequest.readyState == 4  ){
				// Javascript function JSON.parse to parse JSON data
				var jsonObj = JSON.parse(httpRequest.responseText);
				handler(jsonObj)
			}
	}

	httpRequest.open(method, url, true);
	if (method == "POST" || method == "PUT") {
		httpRequest.send();
	} else {
		httpRequest.setRequestHeader("Content-type", "application/json");
		httpRequest.send(body);
	}
}
