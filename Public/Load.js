function loadAll() {
	loadCategories();
	loadTransactions();
}

function loadTransactions() {
	var table = document.getElementById("transactionsTable");

	while (table.hasChildNodes()) {
		table.removeChild(table.firstChild);
	}

	for (var key in transactions) {
		var paidTo=document.createElement("span");
		paidTo.style.position="absolute";
		paidTo.style.top=0;
		paidTo.style.left=10;
		paidTo.innerHTML=transactions[key].paidTo;

if (typeof transactions[key].categoryId !== 'undefined' && typeof categories[transactions[key].categoryId] !== 'undefined') {
	var categoryName=categories[transactions[key].categoryId].name;
	var backgroundColor=categories[transactions[key].categoryId].color;
} else {
	var categoryName = "Unknown";
	var backgroundColor="";
}
		var category=document.createElement("span");
		category.style.paddingRight="40px";
		category.style.float="right";
		category.innerHTML=categoryName;

		var amount=document.createElement("span");
		amount.style.paddingRight="10px";
		amount.style.float="right";
		amount.innerHTML="$" + parseFloat(transactions[key].amount).toFixed(2); // Need to work on localizing the currency

		var date=new Date(transactions[key].transactionDate).toLocaleDateString("en-US"); // Will need to work on localization later
		var transactionDate=document.createElement("span");
		transactionDate.style.paddingRight="10px";
		transactionDate.style.float="right";
		transactionDate.innerHTML=date;

		var editLink=document.createElement("a");
		editLink.href="#";
		eval("editLink.onclick=function() { editTransaction('" + key + "');};");
		editLink.innerHTML="Edit"

		var edit=document.createElement("span");
		edit.style.position="absolute";
		edit.style.top=0;
		edit.style.right=10;
		edit.appendChild(editLink);

		var div=document.createElement("div");
		div.style.backgroundColor=backgroundColor;
		div.className="table_row";
		div.appendChild(paidTo);
		div.appendChild(category);
		div.appendChild(amount);
		div.appendChild(transactionDate);
		div.appendChild(edit);

		var cell=document.createElement("td");
		cell.appendChild(div);

		var row=document.createElement("tr");
		row.style.padding="5px 0";
		row.style.height="28px";
		row.appendChild(cell)

		table.appendChild(row)
	}
}

function loadCategories() {
	var table = document.getElementById("categoriesTable");
	var select = document.getElementById("categorySelect");

	while (table.hasChildNodes()) {
		table.removeChild(table.firstChild);
	}

	while (select.hasChildNodes()) {
		select.removeChild(select.firstChild);
	}

	for (var key in categories) {
		var option=document.createElement("option");
		option.value=key;
		option.innerHTML=categories[key].name;
		select.appendChild(option)

		var name=document.createElement("span");
		name.style.position="absolute";
		name.style.top=0;
		name.style.left=10;
		name.innerHTML=categories[key].name;

		var total=document.createElement("span");
		total.style.paddingRight="40px";
		total.style.float="right";
		total.style.textAlign="right"
		total.style.width="150px";
		total.innerHTML="Total $" + parseFloat(categories[key].amount).toFixed(2); // Need to work on localizing the currency

		var amount=document.createElement("span");
		amount.style.float="right";
		amount.innerHTML="Spent $" + parseFloat(categories[key].total).toFixed(2); // Need to work on localizing the currency

		var editLink=document.createElement("a");
		editLink.href="#";
		eval("editLink.onclick=function() { editCategory('" + key + "');};");
		editLink.innerHTML="Edit"

		var edit=document.createElement("span");
		edit.style.position="absolute";
		edit.style.top=0;
		edit.style.right=10;
		edit.appendChild(editLink);

		var div=document.createElement("div");
		div.style.backgroundColor=categories[key].color;
		div.className="table_row";
		div.appendChild(name);
		div.appendChild(total);
		div.appendChild(amount);
		div.appendChild(edit);

		var cell=document.createElement("td");
		cell.appendChild(div);

		var row=document.createElement("tr");
		row.style.padding="5px 0";
		row.style.height="28px";
		row.appendChild(cell)

		table.appendChild(row)
	}
}
