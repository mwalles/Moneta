function editTransaction(uuid) {
	document.getElementById("transactionEditForm").elements["paidTo"].value=transactions[uuid].paidTo;
	document.getElementById("transactionEditForm").elements["amount"].value=transactions[uuid].amount;
	var date=new Date(transactions[uuid].transactionDate).toLocaleDateString("en-US"); // Will need to work on localization later

	document.getElementById("transactionEditForm").elements["date"].value=date;
	document.getElementById("transactionEditForm").elements["category"].value=transactions[uuid].categoryId;
	document.getElementById("transactionEditForm").elements["uuid"].value=uuid;

	document.getElementById("transactionEditShadow").style.display="inline";
	document.getElementById("transactionEdit").style.display="inline";
}

function editTransactionSave() {
	uuid=document.getElementById("transactionEditForm").elements["uuid"].value;
	transactions[uuid].paidTo=document.getElementById("transactionEditForm").elements["paidTo"].value;
	transactions[uuid].amount=document.getElementById("transactionEditForm").elements["amount"].value;
	var date=new Date(document.getElementById("transactionEditForm").elements["date"].value)
	transactions[uuid].transactionDate=date.toJSON();

	transactions[uuid].categoryId=document.getElementById("transactionEditForm").elements["category"].value;

	loadTransactions();
	editTransactionClose();
}

function editTransactionClose() {
	document.getElementById("transactionEditForm").elements["paidTo"].value="";
	document.getElementById("transactionEditForm").elements["amount"].value="";
	document.getElementById("transactionEditForm").elements["date"].value="";
	document.getElementById("transactionEditForm").elements["category"].value="";
	document.getElementById("transactionEditForm").elements["uuid"].value="";

	document.getElementById("transactionEditShadow").style.display="none";
	document.getElementById("transactionEdit").style.display="none";
}

function deleteCategory() {
	uuid=document.getElementById("categoryEditForm").elements["uuid"].value;
	loadJSON("localhost:8080/api/transaction/1/transaction/" + uuid, "DELETE", JSON.stringify(transactions[uuid]))
}

function editTransactionUpdateReturn(jsonObj) {

}

//------------------------------

function editCategory(uuid) {
	document.getElementById("categoryEditForm").elements["name"].value=categories[uuid].name;
	document.getElementById("categoryEditForm").elements["amount"].value=parseFloat(categories[uuid].amount).toFixed(2);
	document.getElementById("categoryEditForm").elements["color"].value=categories[uuid].color;
	document.getElementById("categoryEditForm").elements["uuid"].value=uuid;
	selectCategoryColor(categories[uuid].color);

	document.getElementById("categoryEditShadow").style.display="inline";
	document.getElementById("categoryEdit").style.display="inline";
}

function editCategorySave() {
	uuid=document.getElementById("categoryEditForm").elements["uuid"].value;
	categories[uuid].name=document.getElementById("categoryEditForm").elements["name"].value;
	categories[uuid].amount=document.getElementById("categoryEditForm").elements["amount"].value;
	categories[uuid].color=document.getElementById("categoryEditForm").elements["color"].value;

	loadJSON("localhost:8080/api/category/1/category/" + uuid, "PATCH", JSON.stringify(transactions[uuid]), editCategoryUpdateReturn)
	loadAll();
	editCategoryClose();
}

function editCategoryUpdateReturn(jsonObj) {

}

function deleteCategory() {
	uuid=document.getElementById("categoryEditForm").elements["uuid"].value;
	loadJSON("localhost:8080/api/category/1/category/" + uuid, "DELETE", JSON.stringify(transactions[uuid]))
}

function editCategoryClose() {
	document.getElementById("categoryEditForm").elements["name"].value="";
	document.getElementById("categoryEditForm").elements["amount"].value="";
	document.getElementById("categoryEditForm").elements["color"].value="";
	document.getElementById("categoryEditForm").elements["uuid"].value="";

	document.getElementById("categoryEditShadow").style.display="none";
	document.getElementById("categoryEdit").style.display="none";
}

function selectCategoryColor(color) {
	var selectedColor = document.getElementById("colorSelection").getElementsByClassName("color_box selected");
	for (key in selectedColor) {
		selectedColor[key].className="color_box"
	}
	document.getElementById(color).className="color_box selected";
	document.getElementById("categoryEditForm").elements["color"].value=color;
}
